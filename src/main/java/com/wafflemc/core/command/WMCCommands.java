package com.wafflemc.core.command;

import com.wafflemc.core.player.DBPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

public class WMCCommands implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String args[]) {

        if (!(sender instanceof Player)) {
            return true;
        }

        Player player = ((Player) sender);

        if (cmd.getName().equalsIgnoreCase("info")) {
            if (args.length == 0) {
                player.sendMessage(ChatColor.RED + "/info <args>");
                return true;
            }
            OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(args[0]);
            UUID uuid = offlinePlayer.getUniqueId();
            DBPlayer db = new DBPlayer(uuid);

            db.sendPlayerData(player);
        }

        return false;
    }
}
