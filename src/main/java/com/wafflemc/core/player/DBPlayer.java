package com.wafflemc.core.player;

import com.wafflemc.core.db.DBConnect;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class DBPlayer {

    private UUID uuid;
    private Player player;
    private OfflinePlayer offlinePlayer;

    private long lastLogin;
    private int bumps;

    public DBPlayer(UUID uuid) {
        try {
            PreparedStatement get = DBConnect.getConnection().prepareStatement("SELECT * FROM playerdata WHERE uuid=?;");
            get.setString(1,uuid.toString());
            ResultSet rs = get.executeQuery();

            if (!rs.next()) {
                DBConnect.createPlayer(uuid);
                if (Bukkit.getPlayer(uuid) != null) {
                    this.player=Bukkit.getPlayer(uuid);
                } else {
                    this.offlinePlayer=Bukkit.getOfflinePlayer(uuid);
                }
            } else {
                lastLogin=Long.parseLong(rs.getString(2));
                bumps=rs.getInt(3);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void bump(UUID uuid) {
        try {

        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public int getBumps() {

        try {
            PreparedStatement get = DBConnect.getConnection().prepareStatement("SELECT * FROM playerdata WHERE uuid=?;");
            get.setString(1,uuid.toString());
            ResultSet rs = get.executeQuery();
            if (rs.next()) {
                return rs.getInt("bumps");
            }
        } catch(Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

    public void sendPlayerData(Player player) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        Date query = new Date(this.lastLogin);
        String date = sdf.format(query);

        player.sendMessage(ChatColor.GREEN + "Last login: " + ChatColor.GRAY + date);
    }

    public void updateLastLogin() {
        try {
            PreparedStatement update = DBConnect.getConnection().prepareStatement("UPDATE playerdata SET lastlogin=? WHERE uuid=?;");
            update.setString(1,System.currentTimeMillis() + "");
            update.setString(2,uuid.toString());
            update.executeUpdate();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
