package com.wafflemc.core;

import com.wafflemc.core.command.WMCCommands;
import com.wafflemc.core.event.JoinEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.Map;

public class WaffleMCCore extends JavaPlugin {

    private static WaffleMCCore plugin;
    private static Map<String,String> loadedOptions;

    @Override
    public void onEnable() {
        plugin=this;

        this.saveDefaultConfig();
        loadedOptions=new HashMap<>();
        loadOptions();

        registerEvent(new JoinEvent());

        getCommand("info").setExecutor(new WMCCommands());

        Bukkit.getMessenger().registerOutgoingPluginChannel(this,"BungeeCord");
    }

    private void registerEvent(Listener... l) {
        for (Listener l1 : l) {
            Bukkit.getPluginManager().registerEvents(l1,this);
        }
    }

    public static WaffleMCCore getPlugin() {
        return plugin;
    }

    private static void loadOptions() {
        String hostname = getPlugin().getConfig().getString("sql.hostname");
        String username = getPlugin().getConfig().getString("sql.username");
        String password = getPlugin().getConfig().getString("sql.password");
        String db = getPlugin().getConfig().getString("sql.db");

        loadedOptions.put("host",hostname);
        loadedOptions.put("user",username);
        loadedOptions.put("pass",password);
        loadedOptions.put("db",db);
    }

    public static Map<String, String> getLoadedOptions() {
        return loadedOptions;
    }
}
