package com.wafflemc.core.event;

import com.wafflemc.core.player.DBPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinEvent implements Listener {

    @EventHandler
    public void joinEvent(PlayerJoinEvent event) {
        new DBPlayer(event.getPlayer().getUniqueId()).updateLastLogin();
    }
}
