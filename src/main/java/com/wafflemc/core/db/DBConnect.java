package com.wafflemc.core.db;

import com.wafflemc.core.WaffleMCCore;
import org.bukkit.Bukkit;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.UUID;

public class DBConnect {

    private String host,username,password,db;
    private static Connection connection;

    public DBConnect() {
        try {
            this.host= WaffleMCCore.getLoadedOptions().get("host");
            this.username=WaffleMCCore.getLoadedOptions().get("user");
            this.password=WaffleMCCore.getLoadedOptions().get("pass");
            this.db=WaffleMCCore.getLoadedOptions().get("db");
            connection=DriverManager.getConnection("jdbc:mysql://" + host + "/" +db,username,password);
            Bukkit.getLogger().info("Connected to database @ " + host);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void createPlayer(UUID uuid) {
        try {
            PreparedStatement insert = connection.prepareStatement("INSERT INTO playerdata values(?,?,?,?);");
            insert.setString(1,uuid.toString());
            insert.setString(2,System.currentTimeMillis()+"");
            insert.setInt(3,0);
            insert.setInt(4,0);
            insert.execute();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() {
        return connection;
    }
}
